# Setup

- git clone git@bitbucket.org:2tunnels/cake.git demo_project
- cd demo_project/app
- composer install
- Prepare your database and database config file
- Import cake.sql (it will use "cake" database, so change that in cake.sql if needed)
- Enjoy :)

# Demo data

## Site users

- email: 2tunnels@gmail.com, pass: qwerty
- email: info@hascoins.com, pass: qwerty

## Paypal sandbox user

- email: hashcoins-test-personal@example.com, pass: qwerty1234

## Vouchers

- **per20** (-20%)
- **tot30** (-30$)
- **free1** (Free product with ID: 1)
- **per50** (-50%)
- **tot50** (-50$)
- **free2** (Free product with ID: 2)
- **per80** (-80%)
