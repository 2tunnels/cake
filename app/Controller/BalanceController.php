<?php

App::uses('AppController', 'Controller');

class BalanceController extends AppController {

    public $uses = ['Transaction'];

    public function index() {
        //
    }

    public function add() {
        // only post method allowed
        $this->request->allowMethod('post');

        // get amount and validate it (better that I did)
        $amount = $this->request->data['Amount'];

        if ($amount < 1) {
            $this->redirectWithFlash(['action' => 'index'], '1$ is a minimum.');
        }

        // get method
        $method = $this->request->data['Method'];

        switch ($method) {
            case 'paypal':
                $this->usePaypal($amount);
                break;

            case 'webmoney':
                $this->useWebmoney();
                break;

            case 'yandex':
                $this->useYandex();
                break;

            default:
                $this->redirectWithFlash(['action' => 'index'], 'Sorry. Choose from available methods.');
                break;
        }
    }

    protected function usePaypal($amount) {
        // create paypal gateway
        // TODO: move this to component or plugin
        $gateway = Omnipay\Omnipay::create('PayPal_Express');
        $gateway->setUsername('hashcoins-test-business_api1.example.com');
        $gateway->setPassword('M43W4PMNDXMYYA62');
        $gateway->setSignature('AhCtVzb.Ch2Q9ssWGRq3idG-XSk.AKlpeeqm54DNEfOOyQL5R2DOdF7e');
        $gateway->setTestMode(true);

        // create transaction record
        $hash = md5(rand(1, 999999));
        $this->Transaction->save([
            'user_id' => $this->Auth->user()['id'],
            'amount' => $amount,
            'hash' => $hash,
            'gateway' => 'paypal',
            'status' => 'created'
        ]);

        $successUrl = Router::url(['controller' => 'paypal', 'action' => 'success', $hash], true);
        $cancelUrl = Router::url(['controller' => 'paypal', 'action' => 'cancel', $hash], true);

        // send request
        $response = $gateway->purchase([
            'amount' => number_format($amount, 2, '.', ''), // by default number will be 1,000.00 and this lib or paypal has trouble with ","
            'currency' => 'USD',
            'description' => 'Add funds',
            'returnUrl' => $successUrl,
            'cancelUrl' => $cancelUrl
        ])->send();

        // handle response
        if ($response->isRedirect()) {
            $response->redirect();
        } else {
            $this->redirectWithFlash(['action' => 'index'], 'Something went wrong with Paypal gateway.');
        }
    }

    protected function useWebmoney() {
        die('Webmoney is not implemented yet.');
    }

    protected function useYandex() {
        die('Yandex.Money is not implemented yet.');
    }

}
