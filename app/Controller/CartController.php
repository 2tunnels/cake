<?php

App::uses('AppController', 'Controller');

class CartController extends AppController {

    public $uses = ['User', 'Voucher'];

    public function index() {
        $products = $this->Cart->getProducts();

        $this->set('products', $products);
    }

    public function pay() {
        // only post method allowed
        $this->request->allowMethod('post');

        // check if user can buy everything
        if ($this->Auth->user()['balance'] < $this->Cart->getProductsTotalPrice()) {
            $this->redirectWithFlash([
                'controller' => 'balance',
                'action' => 'index'
            ], 'You are too poor. But you can increase your balance!');
        }

        // get products from cart
        $products = $this->Cart->getProducts();

        // do something with them

        // decrease balance
        $newBalance = $this->Auth->user()['balance'] - $this->Cart->getProductsTotalPrice();

        $this->User->id = $this->Auth->user()['id'];
        $this->User->set('balance', $newBalance);
        $this->User->save();

        // sync model and session
        $this->Session->write('Auth.User.balance', $newBalance);

        // set voucher as used
        $this->Voucher->id = $this->Cart->getVoucherId();
        $this->Voucher->set('used', true);
        $this->Voucher->save();

        // delete products and voucher from cart
        $this->Cart->destroy();

        // redirect to products page
        $this->redirectWithFlash([
            'controller' => 'products',
            'action' => 'index'
        ], 'Items were purchased and something happened...');
    }

    public function addVoucher() {
        // only post method allowed
        $this->request->allowMethod('post');

        $voucherCode = $this->request->data['Voucher'];

        if (!$voucherCode) {
            $this->redirectWithFlash(['action' => 'index'], 'Empty voucher will not work. Trust me.');
        }

        $voucher = $this->Voucher->find('first', [
            'conditions' => [
                'code' => $voucherCode,
                'used' => false
            ]
        ]);

        if (!$voucher) {
            $this->redirectWithFlash(['action' => 'index'], 'Cannot find voucher like that.');
        }

        $this->Cart->setVoucher($voucher);

        $this->redirect(['action' => 'index']);
    }

}
