<?php

App::uses('Component', 'Component');

// Silly cart implementation
class CartComponent extends Component {

    const VOUCHER_TYPE_PERCENTAGE = 'percentage';
    const VOUCHER_TYPE_TOTAL = 'total';
    const VOUCHER_TYPE_FREE = 'free';

    protected $productsSessionKey = 'Cart.products';
    protected $voucherSessionKey = 'Cart.voucher';

    public $components = [
        'Session'
    ];

    /*
     *  Products
     */

    // Check if cart has any products
    public function hasProducts() {
        return $this->Session->check($this->productsSessionKey);
    }

    // Check if cart has particular product
    public function hasProduct($productId) {
        return $this->Session->check($this->productsSessionKey . '.' . $productId);
    }

    // Get all products
    public function getProducts() {
        if (!$this->hasProducts()) {
            return null;
        }

        // get products from session
        $products = $this->Session->read($this->productsSessionKey);

        // add new price if voucher were used
        if ($this->hasVoucher()) {
            if ($this->isVourcherType(static::VOUCHER_TYPE_PERCENTAGE)) {
                foreach ($products as $key => $product) {
                    $discount = $products[$key]['price'] / 100 * $this->getVoucherValue();
                    $newPrice = $products[$key]['price'] - $discount;
                    $products[$key]['new_price'] = $newPrice;
                }
            } elseif ($this->isVourcherType(static::VOUCHER_TYPE_FREE)) {
                $freeProductId = $this->getVoucherValue();
                foreach ($products as $key => $product) {
                    if ($product['id'] == $freeProductId) {
                        $products[$key]['new_price'] = 0;
                    }
                }
            }
        }

        return $products;
    }

    // Get particular product
    public function getProduct($productId) {
        if (!$this->hasProducts()) {
            return null;
        }

        if (!$this->hasProduct($productId)) {
            return null;
        }

        return $this->Session->read($this->productsSessionKey . '.' . $productId);
    }

    // Count products
    public function getProductsQuantity() {
        if (!$this->hasProducts()) {
            return 0;
        }

        $quantity = 0;

        foreach ($this->getProducts() as $product) {
            $quantity += $product['quantity'];
        }

        return $quantity;
    }

    // Add product
    public function addProduct($product) {
        // check if current item is already in cart
        // if so change quantity
        // else just put it in
        $productId = $product['Product']['id'];
        if ($this->hasProduct($productId)) {
            $item = $this->getProduct($productId);
            $item['quantity'] += 1;
            $this->Session->write($this->productsSessionKey . '.' . $productId, $item);
        } else {
            $this->Session->write($this->productsSessionKey . '.' . $productId, [
                'id' => $product['Product']['id'],
                'title' => $product['Product']['title'],
                'price' => $product['Product']['price'],
                'quantity' => 1
            ]);
        }
    }

    // Get total price of all products
    public function getProductsTotalPrice() {
        $total = 0;

        foreach ($this->getProducts() as $product) {
            if (isset($product['new_price'])) {
                $total += $product['new_price'] * $product['quantity'];
            } else {
                $total += $product['price'] * $product['quantity'];
            }
        }

        if ($this->hasVoucher() && $this->isVourcherType(static::VOUCHER_TYPE_TOTAL)) {
            $total -= $this->getVoucherValue();
            if ($total < 0) {
                $total = 0;
            }
        }

        return $total;
    }

    /*
     *  Voucher
     */

    // Check if cart has used voucher
    public function hasVoucher() {
        return $this->Session->check($this->voucherSessionKey);
    }

    // Set voucher
    // TODO: throw exception if voucher is already set
    public function setVoucher($voucher) {
        if ($this->hasVoucher()) {
            throw new Exception('You cant set another voucher');
        }

        $this->Session->write($this->voucherSessionKey, [
            'id' => $voucher['Voucher']['id'],
            'code' => $voucher['Voucher']['code'],
            'type' => $voucher['Voucher']['type'],
            'value' => $voucher['Voucher']['value']
        ]);
    }

    // Get voucher
    public function getVoucher() {
        return $this->Session->read($this->voucherSessionKey);
    }

    // Get voucher ID
    public function getVoucherId() {
        if (!$this->hasVoucher()) {
            return null;
        }

        return $this->Session->read($this->voucherSessionKey)['id'];
    }

    // Get voucher code
    public function getVoucherCode() {
        if (!$this->hasVoucher()) {
            return null;
        }

        return $this->Session->read($this->voucherSessionKey)['code'];
    }

    // Get voucher type
    public function getVoucherType() {
        if (!$this->hasVoucher()) {
            return null;
        }

        return $this->Session->read($this->voucherSessionKey)['type'];
    }

    // Get voucher value
    public function getVoucherValue() {
        if (!$this->hasVoucher()) {
            return null;
        }

        return $this->Session->read($this->voucherSessionKey)['value'];
    }

    // Check voucher type
    public function isVourcherType($type) {
        if (!$this->hasVoucher()) {
            return false;
        }

        return $type == $this->getVoucherType();
    }

    // Delete everything from cart
    public function destroy() {
        $this->Session->delete($this->productsSessionKey);
        $this->Session->delete($this->voucherSessionKey);
    }

}
