<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController {

    public function login() {
        // if user already logged in, redirect to home page
        if ($this->Auth->loggedIn()) {
            $this->redirect(['controller' => 'products', 'action' => 'index']);
        }

        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->redirectWithFlash($this->Auth->redirectUrl(), 'Welcome back!');
            } else {
                $this->redirectWithFlash($this->Auth->loginAction, 'Nope. Wrong email or password. Or both...');
            }
        }
    }

    public function logout() {
        $this->request->allowMethod('post');

        return $this->redirect($this->Auth->logout());
    }

}
