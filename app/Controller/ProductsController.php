<?php

App::uses('AppController', 'Controller');

class ProductsController extends AppController {

    public function beforeFilter() {
        $this->Auth->allow(['index']);
    }

    public function index() {
        $products = $this->Product->find('all');

        $this->set('products', $products);
    }

    public function addToCart($productId) {
        // if user was redirected back from login page
        if ($this->request->is('get')) {
            $this->redirect(['action' => 'index']);
        }

        // only post method allowed
        $this->request->allowMethod('post');

        // get product
        $product = $this->Product->findById($productId);

        // 404 if product not found
        if (!$product) {
            throw new NotFoundException('Could not find product with ID: ' . $productId);
        }

        // add product to cart
        $this->Cart->addProduct($product);

        // redirect back
        $this->redirect($this->referer(['action' => 'index']));
    }

}
