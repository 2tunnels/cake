<?php

App::uses('AppController', 'Controller');

class PaypalController extends AppController {

    public $uses = ['Transaction', 'User'];

    public function success($hash) {
        // find transaction in db
        $transaction = $this->Transaction->find('first', [
            'conditions' => [
                'hash' => $hash,
                'status' => 'created'
            ]
        ]);

        // 404 if transaction not found
        if (!$transaction) {
            throw new NotFoundException('Cant find transaction with hash: ' . $hash);
        }

        // update transaction status
        $this->Transaction->id = $transaction['Transaction']['id'];
        $this->Transaction->set('status', 'success');
        $this->Transaction->save();

        // add balance to user
        $user = $this->User->findById($transaction['Transaction']['user_id']);
        $newBalance = $user['User']['balance'] + $transaction['Transaction']['amount'];
        $this->User->id = $user['User']['id'];
        $this->User->set('balance', $newBalance);
        $this->User->save();

        // sync model and session
        $this->Session->write('Auth.User.balance', $newBalance);

        $this->redirectWithFlash([
            'controller' => 'balance',
            'action' => 'index'
        ], 'Balance was increased!');
    }

    public function cancel($hash) {
        $transaction = $this->Transaction->find('first', [
            'conditions' => [
                'hash' => $hash,
                'status' => 'created'
            ]
        ]);

        if (!$transaction) {
            throw new NotFoundException('Cant find transaction with hash: ' . $hash);
        }

        $this->Transaction->id = $transaction['Transaction']['id'];
        $this->Transaction->set('status', 'canceled');
        $this->Transaction->save();

        $this->redirectWithFlash([
            'controller' => 'balance',
            'action' => 'index'
        ], 'Transaction was cancelled.');
    }

}
