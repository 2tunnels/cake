<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            Your cart
        </div>
        <?php if (!$cart->hasProducts()): ?>
            <div class="panel-body">
                <p>No products were added to cart yet.</p>
            </div>
        <?php else: ?>
            <table class="table">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Price per item</th>
                        <th>Quantity</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($cart->getProducts() as $product): ?>
                        <tr>
                            <td>
                                <?= h($product['title']) ?>
                            </td>
                            <td>
                                <?php if (isset($product['new_price'])): ?>
                                    <span style="text-decoration:line-through;">
                                        <?= h($product['price']) ?>$
                                    </span>
                                    &nbsp;
                                    <b>
                                        <?= h($product['new_price']) ?>$
                                    </b>
                                <?php else: ?>
                                    <?= h($product['price']) ?>$
                                <?php endif; ?>
                            </td>
                            <td>
                                <?= h($product['quantity']) ?>
                            </td>
                            <td>
                                <?php if (isset($product['new_price'])): ?>
                                    <?= h($product['new_price'] * $product['quantity']) ?>$
                                <?php else: ?>
                                    <?= h($product['price'] * $product['quantity']) ?>$
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td colspan="3"></td>
                        <th>
                            <?= h($cart->getProductsTotalPrice()) ?>$
                        </th>
                    </tr>
                </tbody>
            </table>
            <div class="panel-body">
                <div class="well">
                    <?php if ($cart->hasVoucher()): ?>
                        <b class="lead">
                            <?= h($cart->getVoucherCode()) ?>:
                            <?php if ($cart->isVourcherType('percentage')): ?>
                                -<?= h($cart->getVoucherValue()) ?>%
                            <?php elseif ($cart->isVourcherType('total')): ?>
                                -<?= h($cart->getVoucherValue()) ?>$
                            <?php elseif ($cart->isVourcherType('free')): ?>
                                Free <?= h($cart->getProduct($cart->getVoucherValue())['title']) ?>
                            <?php endif; ?>
                        </b>
                    <?php else: ?>
                        <p>
                            If you have any vouchers it's good time to use them!
                        </p>
                        <?php
                        echo $this->Form->create(false, ['action' => 'addVoucher', 'class' => 'form-inline']);
                        echo $this->Form->input('Voucher', [
                            'label' => [
                                'class' => 'sr-only'
                            ],
                            'placeholder' => 'Voucher',
                            'class' => 'form-control',
                            'div' => [
                                'class' => 'form-group'
                            ]
                        ]);
                        echo ' '; // yes, this is important! :D
                        echo $this->Form->submit('Use', [
                            'div' => false,
                            'class' => 'btn btn-default'
                        ]);
                        echo $this->Form->end();
                        ?>
                    <?php endif; ?>
                </div>
                <div class="text-right">
                    <?php
                    echo $this->Form->postLink('Pay', [
                        'action' => 'pay'
                    ], [
                        'class' => 'btn btn-success btn-lg'
                    ]);
                    ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
