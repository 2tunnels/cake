<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            Login
        </div>
        <div class="panel-body">
            <?php
            echo $this->Form->create('User');
            echo $this->Form->input('email', [
                'div' => 'form-group',
                'class' => 'form-control'
            ]);
            echo $this->Form->input('password', [
                'div' => 'form-group',
                'class' => 'form-control'
            ]);
            echo $this->Form->submit('Login', [
                'class' => 'btn btn-default'
            ]);
            echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
