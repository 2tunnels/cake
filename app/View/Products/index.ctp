<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            Products
        </div>
        <div class="panel-body">
            <?php if (!$products): ?>
                <p class="text-center">No products yet.</p>
            <?php else: ?>
                <div class="row">
                    <?php foreach ($products as $product): ?>
                        <div class="col-sm-4 col-lg-3">
                            <div class="thumbnail">
                                <img src="http://placehold.it/300x300" alt="">
                                <div class="caption">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <?= h($product['Product']['title']) ?>
                                        </div>
                                        <div class="col-xs-6 text-right">
                                            <b><?= h($product['Product']['price']) ?>$</b>
                                        </div>
                                    </div>
                                    <p>
                                        <?php
                                        echo $this->Form->postLink('Add to cart', [
                                            'action' => 'addToCart',
                                            $product['Product']['id']
                                        ], [
                                            'class' => 'btn btn-primary btn-block'
                                        ]);
                                        ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
