<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo $this->Html->url(['controller' => 'products', 'action' => 'index']); ?>">Cake Demo</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <?php echo $this->Html->link('Products', ['controller' => 'products', 'action' => 'index']); ?>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if ($user_logged_in): ?>
                    <li>
                        <?php echo $this->Html->link(sprintf('Cart (%s)', $cart->getProductsQuantity()), ['controller' => 'cart', 'action' => 'index']); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link(sprintf('Balance (%s$)', $user['balance']), ['controller' => 'balance', 'action' => 'index']); ?>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <?php echo h($user['email']); ?> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <?php echo $this->Form->postLink('Logout', ['controller' => 'users', 'action' => 'logout']); ?>
                            </li>
                        </ul>
                    </li>
                <?php else: ?>
                    <li>
                        <?php echo $this->Html->link('Login', ['controller' => 'users', 'action' => 'login']); ?>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>
