<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            Your balance
        </div>
        <div class="panel-body">
            <?= h($user['balance']) ?>$
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            Add funds
        </div>
        <div class="panel-body">
            <?php
            echo $this->Form->create(false, ['action' => 'add']);
            echo $this->Form->input('Amount', [
                'div' => 'form-group',
                'class' => 'form-control',
                'value' => '100'
            ]);
            echo $this->Form->input('Method', [
                'div' => 'form-group',
                'class' => 'form-control',
                'options' => [
                    'paypal' => 'PayPal',
                    'webmoney' => 'Webmoney',
                    'yandex' => 'Yandex.Money'
                ]
            ]);
            echo $this->Form->submit('Add funds', [
                'class' => 'btn btn-success'
            ]);
            echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
